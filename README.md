# MEALPLANNER

Mealplanner is a responsive web app that lets users create an account, search for recipes and adding them to a personal meal plan. Project is created using React, JavaScript, MaterialUI, JSON server and a Spoonacular API.

## Features

-   Create a user account
-   Login/Logout
-   Search for recipes
-   Add recipes to a meal plan with the following parameters:
    -   Pick a date from the calendar
    -   Meal type (Breakfast, Lunch, Dinner, Snack)
-   Removing recipes from the meal plan
-   Only a logged in user can save recipes to a meal plan and see the My Plan page
-   Logged in user can only see their own meal plan which is protected with JSON Server Auth

## Libraries used

-   React
-   Material UI
-   Axios
-   JSON Server
-   JSON Server Auth

## Example images of basic functionality

Homepage ![](ReadMeImages/homepage.jpg)

Account creation modal ![](ReadMeImages/registration-modal.jpg)

Login modal ![](ReadMeImages/login-modal.jpg)

Search results ![](ReadMeImages/search-results.jpg)

Expanded recipe card showing cooking instructions ![](ReadMeImages/search-result-expanded.jpg)

Add to plan window asking for meal type and date ![](ReadMeImages/add-to-plan-modal.jpg)

My plan view listing added recipes with details chosen by the user ![](ReadMeImages/my-plan.jpg)

## Mobile version example images

![](ReadMeImages/mealplanner-mobile.jpg)

## Install & run instructions:

-   Download and install Node.js from https://nodejs.org/en/download/
-   Download and install "git" from https://git-scm.com/downloads
-   In your console navigate to the folder where you want the project and run the following commands:
    -   git clone https://gitlab.com/MaximusAnakin/mealplanner.git
    -   cd mealplanner
    -   npm install
    -   npm start
-   In the root folder of the project create a "db.json" file and add the following code including all brackets: 
        {
        "users": [],
        "meals": [],
        "recipes": []
        }
-   Open a second console window and run "npm run server"
-   Signup at https://spoonacular.com/food-api/console
-   In your Spoonacular API dashboard go to profile and copy your API key.
-   In the root folder of the project create ".env" file and add "REACT_APP_SPOONACULAR_API_KEY={yourkey}". Paste your API key inside the curly brackets and do NOT include the brackets.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.
