import { createTheme } from "@mui/material";

const theme = createTheme({
	palette: {
		primary: {
			main: "#48D400",
		},
		secondary: {
			main: "#5CFF08"
		}
	}
});

export default theme;
