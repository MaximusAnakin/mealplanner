import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { UserContext } from "./UserContext";

const ProtectedRoute = ({ children }) => {
	const { userId } = useContext(UserContext);

	if (userId === null) return <Navigate to="/" replace />;

	return children;
};

export default ProtectedRoute;
