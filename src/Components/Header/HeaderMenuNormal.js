import React, { useContext } from "react";
import { Box, Button } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../UserContext";

const HeaderMenuNormal = ({ pages }) => {
	const {userId, logout, setShowLoginModal} = useContext(UserContext);
	const navigate = useNavigate();

	return (
		<>
			<Box
				sx={{
					display: { xs: "none", md: "flex" },
					width: "100%",
					justifyContent: "end"
				}}
			>
				{userId !== null &&
					pages.map((page) => (
						<Button
							key={page.name}
							component={Link}
							to={page.path}
							sx={{ color: "white" }}
						>
							{page.name}
						</Button>
					))}
				{userId !== null && (
					<Button
						variant="contained"
						color="secondary"
						onClick={() => {
							logout();
							navigate("/");
						}}
					>
						Logout
					</Button>
				)}
				{userId === null && (
					<Button
						variant="contained"
						size="small"
						color="warning"
						onClick={() => setShowLoginModal(true)}
					>
						Login
					</Button>
				)}
			</Box>
		</>
	);
};

export default HeaderMenuNormal;
