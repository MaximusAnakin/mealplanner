import React from "react";
import { useNavigate } from "react-router-dom";
import RestaurantIcon from "@mui/icons-material/Restaurant";
import { Typography } from "@mui/material";

const LogoText = ({ mobile }) => {
	const navigate = useNavigate();

	const styleLarge = {
		cursor: "pointer",
		mr: 2,
		display: { xs: "none", md: "flex" }
	};

	const styleMobile = {
		cursor: "pointer",
		flexGrow: 1,
		justifyContent: "center",
		display: { xs: "flex", md: "none" }
	};
    
	return (
		<>
			{!mobile && (
				<Typography
					variant="h6"
					sx={styleLarge}
					onClick={() => navigate("/")}
				>
					MEALPLANNER <RestaurantIcon />
				</Typography>
			)}
			{mobile && (
				<Typography
					variant="h6"
					sx={styleMobile}
					onClick={() => navigate("/")}
				>
					MEALPLANNER <RestaurantIcon />
				</Typography>
			)}
		</>
	);
};
export default LogoText;
