import React, { useContext, useState } from "react";
import MenuIcon from "@mui/icons-material/Menu";
import { Link, useNavigate } from "react-router-dom";
import {
	Box,
	IconButton,
	Typography,
	Menu,
	MenuItem,
	Button
} from "@mui/material";
import { UserContext } from "../UserContext";

const HeaderMenuMobile = ({ pages }) => {
	const [anchorElNav, setAnchorElNav] = useState(null);
	const { userId, logout, setShowLoginModal } = useContext(UserContext);
	const navigate = useNavigate();

	const handleOpenNavMenu = (event) => {
		setAnchorElNav(event.currentTarget);
	};

	const handleCloseNavMenu = () => {
		setAnchorElNav(null);
	};

	return (
		<>
			<Box sx={{ display: { xs: "flex", md: "none" } }}>
				<IconButton
					size="large"
					onClick={handleOpenNavMenu}
					color="inherit"
				>
					<MenuIcon />
				</IconButton>
				<Menu
					id="menu-appbar"
					anchorEl={anchorElNav}
					anchorOrigin={{
						vertical: "bottom",
						horizontal: "left"
					}}
					keepMounted
					transformOrigin={{
						vertical: "top",
						horizontal: "left"
					}}
					open={Boolean(anchorElNav)}
					onClose={handleCloseNavMenu}
					sx={{ display: "block", textAlign: "center" }}
				>
					{userId === null && (
						<Button
							color="warning"
							onClick={() => setShowLoginModal(true)}
						>
							Login
						</Button>
					)}
					{userId !== null &&
						pages.map((page) => (
							<MenuItem
								key={page.name}
								onClick={handleCloseNavMenu}
								component={Link}
								to={page.path}
							>
								<Typography textAlign="center">
									{page.name}
								</Typography>
							</MenuItem>
						))}
					{userId !== null && (
						<Button
							color="primary"
							onClick={() => {
								logout();
								navigate("/");
							}}
						>
							Logout
						</Button>
					)}
				</Menu>
			</Box>
		</>
	);
};
export default HeaderMenuMobile;
