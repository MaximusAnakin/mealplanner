import React from "react";
import LogoText from "./LogoText";
import HeaderMenuNormal from "./HeaderMenuNormal";
import HeaderMenuMobile from "./HeaderMenuMobile";
import { AppBar, Toolbar } from "@mui/material";

const pages = [{ name: "My plan", path: "/myplan" }];

const Header = () => (
	<AppBar position="relative">
		<Toolbar sx={{ px: 3 }}>
			<HeaderMenuMobile
				pages={pages}
			/>
			<LogoText mobile={true} />
			<LogoText />
			<HeaderMenuNormal
				pages={pages}
			/>
		</Toolbar>
	</AppBar>
);
export default Header;
