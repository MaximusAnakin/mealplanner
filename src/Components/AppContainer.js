import React from "react";
import { BrowserRouter } from "react-router-dom";
import Router from "./Router";
import Header from "./Header/Header";
import Login from "./Home/Login/Login";
import CreateAccount from "./Home/CreateAccount/CreateAccount";

const AppContainer = () => (
	<>
		<Login />
		<CreateAccount />
		<BrowserRouter>
			<Header />
			<Router />
		</BrowserRouter>
	</>
);
export default AppContainer;
