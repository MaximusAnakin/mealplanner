import React, { useContext, useState } from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
	Card,
	CardHeader,
	CardMedia,
	CardContent,
	Collapse,
	IconButton,
	Typography,
	Stack,
	styled,
	Button
} from "@mui/material";
import AddToPlan from "./AddToPlan/AddToPlan";
import { UserContext } from "../UserContext";

const ExpandMore = styled((props) => {
	const { ...other } = props;
	return <IconButton {...other} />;
})(({ theme, expand }) => ({
	transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
	marginLeft: "auto",
	transition: theme.transitions.create("transform", {
		duration: theme.transitions.duration.shortest
	})
}));

const RecipeCard = ({
	title,
	recipeId,
	image,
	steps,
	time,
	servings}) => {
	const [expanded, setExpanded] = useState(false);
	const [showAddToPlanModal, setShowAddToPlanModal] = useState(false);
	const {userId, setShowLoginModal} = useContext(UserContext);

	const handleExpandClick = () => {
		setExpanded(!expanded);
	};

	const stepList = steps.map((step) => step.step + " ");

	return (
		<>
			<Card sx={{ width: { xs: "350px", sm: "500px" }, margin: "20px" }}>
				<CardHeader
					title={title}
					sx={{ bgcolor: "primary.dark", color: "white" }}
				/>
				<Stack direction={{ xs: "column", sm: "row" }}>
					<CardMedia
						component="img"
						sx={{ width: "350px" }}
						image={image}
						alt={title + " recipe"}
					/>
					<div
						style={{
							display: "flex",
							flexDirection: "column",
							justifyContent: "space-between"
						}}
					>
						<Stack spacing={1} sx={{ p: 2 }}>
							<Typography variant="body2" color="text.secondary">
								Time: <b>{time}</b>
							</Typography>
							<Typography variant="body2" color="text.secondary">
								Servings: <b>{servings}</b>
							</Typography>
							{userId === null && (
								<Button
									variant="contained"
									size="small"
									color="warning"
									onClick={() => setShowLoginModal(true)}
								>
									Login to add
								</Button>
							)}
							{userId !== null && (
								<Button
									variant="contained"
									size="small"
									onClick={() => setShowAddToPlanModal(true)}
								>
									Add to plan
								</Button>
							)}
						</Stack>

						<ExpandMore
							expand={expanded ? 1 : 0}
							onClick={handleExpandClick}
							aria-expanded={expanded}
							aria-label="show more"
							sx={{ alignSelf: "end", m: 0 }}
						>
							<ExpandMoreIcon />
						</ExpandMore>
					</div>
				</Stack>
				<Collapse in={expanded} timeout="auto" unmountOnExit>
					<CardContent>
						<Typography>{stepList}</Typography>
					</CardContent>
				</Collapse>
			</Card>
			<AddToPlan
				showAddToPlanModal={showAddToPlanModal}
				setShowAddToPlanModal={setShowAddToPlanModal}
				title={title}
				image={image}
				stepList={stepList}
				recipeId={recipeId}
			/>
		</>
	);
};

export default RecipeCard;
