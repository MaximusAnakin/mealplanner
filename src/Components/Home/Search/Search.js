import React, { useState } from "react";
import { Button, Stack, TextField } from "@mui/material";
import axios from "axios";

const Search = ({ setSearchResults }) => {
	const [searchTerm, setSearchTerm] = useState("");
	const [resultAmount, setResultAmount] = useState(0);

	// eslint-disable-next-line no-undef
	const apikey = process.env.REACT_APP_SPOONACULAR_API_KEY;

	const getRecipes = (event) => {
		event.preventDefault();
		axios({
			method: "get",
			url: `https://api.spoonacular.com/recipes/complexSearch?apiKey=${apikey}`,
			headers: {
				"Content-type": "application/json; charset=utf-8"
			},
			params: {
				query: searchTerm,
				instructionsRequired: true,
				addRecipeInformation: true
			}
		})
			.then((res) => {
				if (res.data.totalResults === 0)
					alert(
						`We did not find anything with "${searchTerm}". Please, try something else.`
					);
				setResultAmount(res.data.totalResults);
				setSearchResults(res.data.results);
			})
			.catch(() => alert("Something went wrong. Please try again later."));
	};

	const handleSearchChange = (event) => {
		setSearchTerm(event.target.value);
	};

	return (
		<>
			<form onSubmit={getRecipes}>
				<Stack
					margin="30px"
					width={{ xs: "350px", sm: "500px" }}
					direction="row"
				>
					<TextField
						fullWidth
						label="Search recipes by ingredient"
						size="small"
						value={searchTerm}
						onChange={handleSearchChange}
					/>
					<Button type="submit">Search</Button>
				</Stack>
			</form>
			{resultAmount !== 0 && (
				<h5 style={{ textAlign: "center" }}>
					We found {resultAmount} results for your search.
				</h5>
			)}
		</>
	);
};

export default Search;
