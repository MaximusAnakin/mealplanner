import React, { useContext } from "react";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import TextField from "@mui/material/TextField";
import axios from "axios";
import {
	InputLabel,
	MenuItem,
	FormControl,
	Select,
	Stack,
	Button
} from "@mui/material";
import { UserContext } from "../../UserContext";

const AddToPlanForm = ({
	title,
	image,
	stepList,
	closeModal,
	type,
	dateISO,
	setDateISO,
	setType,
	recipeId
}) => {
	const { userId, accessToken } = useContext(UserContext);

	const handleTypeChange = (event) => {
		setType(event.target.value);
	};

	const handleDateChange = (newValue) => {
		setDateISO(newValue);
	};

	const dateStringFormat = {
		weekday: "long",
		day: "numeric",
		month: "long"
	};

	const saveRecipe = () => {
		axios
			.post("http://localhost:3001/recipes/", {
				id: recipeId,
				title,
				image,
				stepList,
				userId
			})
			.catch(() => {
				console.log("Error: Can't save duplicate recipes in Json-server. Should be fixed in the backend.");
			});
	};

	const saveMeal = () => {
		axios({
			method: "post",
			url: "http://localhost:3001/meals/",
			data: {
				userId,
				recipeId,
				dateISO,
				type,
				date: dateISO.toLocaleString("en-GB", dateStringFormat)
			},
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/JSON; charset=utf-8"
			}
		}).catch(() => {
			alert("Something went wrong.");
		});
	};

	const handleAddToPlan = (e) => {
		e.preventDefault();
		Promise.all([saveRecipe(), saveMeal()])
			.then(() => {
				closeModal();
			})
			.catch(() => {
				alert("Something went wrong");
			});
	};

	return (
		<>
			<form onSubmit={handleAddToPlan}>
				<Stack spacing={2}>
					<FormControl fullWidth>
						<InputLabel>Meal type</InputLabel>
						<Select
							value={type}
							label="Meal type"
							onChange={handleTypeChange}
							required
						>
							<MenuItem value={"Breakfast"}>Breakfast</MenuItem>
							<MenuItem value={"Lunch"}>Lunch</MenuItem>
							<MenuItem value={"Dinner"}>Dinner</MenuItem>
							<MenuItem value={"Snack"}>Snack</MenuItem>
						</Select>
					</FormControl>
					<LocalizationProvider dateAdapter={AdapterDateFns}>
						<DesktopDatePicker
							label="Date"
							inputFormat="dd/MM/yyyy"
							value={dateISO}
							onChange={handleDateChange}
							renderInput={(params) => <TextField {...params} />}
						/>
					</LocalizationProvider>
					<Button variant="contained" type={"submit"}>
						Add to plan
					</Button>
				</Stack>
			</form>
		</>
	);
};
export default AddToPlanForm;
