import React, { useState } from "react";
import CancelIcon from "@mui/icons-material/Cancel";
import { Box, Modal, IconButton } from "@mui/material";
import AddToPlanForm from "./AddToPlanForm";

const AddToPlan = ({
	showAddToPlanModal,
	setShowAddToPlanModal,
	title,
	image,
	stepList,
	recipeId
}) => {
	const [type, setType] = useState("");
	const [dateISO, setDateISO] = useState(new Date());

	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		width: { xs: "60%", sm: "auto" },
		transform: "translate(-50%, -50%)",
		p: 4,
		boxShadow: 5,
		bgcolor: "#fff"
	};

	const closeModal = () => {
		setShowAddToPlanModal(false);
		setType("");
	};

	return (
		<Modal
			open={showAddToPlanModal}
			onClose={closeModal}
			disableScrollLock={true}
		>
			<Box sx={modalStyle}>
				<IconButton
					onClick={closeModal}
					tabIndex={-1}
					style={{
						position: "absolute",
						right: 0,
						top: 0
					}}
				>
					<CancelIcon />
				</IconButton>
				<AddToPlanForm
					stepList={stepList}
					title={title}
					image={image}
					type={type}
					dateISO={dateISO}
					setType={setType}
					setDateISO={setDateISO}
					closeModal={closeModal}
					recipeId={recipeId}
				/>
			</Box>
		</Modal>
	);
};

export default AddToPlan;
