import {
	Box,
	Button,
	IconButton,
	Modal,
	Stack,
	TextField
} from "@mui/material";
import axios from "axios";
import React, { useContext, useState } from "react";
import CancelIcon from "@mui/icons-material/Cancel";
import { UserContext } from "../../UserContext";

const Login = () => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const { showLoginModal, setShowLoginModal, setCreateAccountModal, login } =
		useContext(UserContext);

	const handleLogin = (event) => {
		event.preventDefault();
		axios
			.post("http://localhost:3001/login", {
				email,
				password
			})
			.then((res) => {
				login(res.data.user.id, res.data.accessToken);
				setEmail("");
				setPassword("");
			})
			.catch(() => alert("Something went wrong."));
	};


	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		width: { xs: "60%", sm: "auto" },
		transform: "translate(-50%, -50%)",
		p: 4,
		boxShadow: 5,
		bgcolor: "#fff"
	};

	const closeModal = () => {
		setShowLoginModal(false);
		setEmail("");
		setPassword("");
	};

	const switchToRegistration = () => {
		closeModal();
		setCreateAccountModal(true);
	};

	return (
		<Modal
			open={showLoginModal}
			onClose={closeModal}
			disableScrollLock={true}
		>
			<Box sx={modalStyle}>
				<IconButton
					onClick={closeModal}
					tabIndex={-1}
					style={{
						position: "absolute",
						right: 0,
						top: 0
					}}
				>
					<CancelIcon />
				</IconButton>
				<form onSubmit={handleLogin}>
					<Stack spacing={2}>
						<h3>Login</h3>
						<TextField
							required
							label="Email"
							value={email}
							onChange={({ target }) => setEmail(target.value)}
						/>
						<TextField
							required
							label="Password"
							type="password"
							value={password}
							onChange={({ target }) => setPassword(target.value)}
						/>
						<Button variant="contained" type="submit">
							Login
						</Button>
					</Stack>
				</form>
				<Stack
					sx={{
						paddingTop: "10px",
						flexDirection: { xs: "column", md: "row" },
						alignItems: "center"
					}}
				>
					<h5>No account yet?</h5>
					<Button size="small" onClick={switchToRegistration}>
						Create account
					</Button>
				</Stack>
			</Box>
		</Modal>
	);
};

export default Login;
