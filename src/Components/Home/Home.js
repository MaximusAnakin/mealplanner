import { Stack } from "@mui/material";
import React, { useState } from "react";
import RecipeCard from "./RecipeCard";
import Search from "./Search/Search";

const welcome = {
	padding: "30px",
	textAlign: "center"
};

const Home = () => {
	const [searchResults, setSearchResults] = useState([]);

	return (
		<Stack sx={{ alignItems: "center" }}>
			{searchResults.length === 0 && (
				<div style={welcome}>
					<h1>Welcome to MealPlanner</h1>
					<h4>Search for recipes and add to your plan</h4>
				</div>
			)}

			<div style={{ textAlign: "center" }}>
				<Search setSearchResults={setSearchResults} />
			</div>

			{searchResults.map((result) => (
				<RecipeCard
					key={result.id}
					recipeId={result.id}
					title={result.title}
					image={result.image}
					steps={result.analyzedInstructions[0].steps}
					time={result.readyInMinutes}
					servings={result.servings}
				/>
			))}
		</Stack>
	);
};

export default Home;
