import {
	Box,
	Button,
	IconButton,
	Modal,
	Stack,
	TextField
} from "@mui/material";
import axios from "axios";
import React, { useContext, useState } from "react";
import CancelIcon from "@mui/icons-material/Cancel";
import { UserContext } from "../../UserContext";

const CreateAccount = () => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const {
		createAccountModal,
		setCreateAccountModal,
		setShowLoginModal,
		createAccount
	} = useContext(UserContext);

	const handleCreateAccount = (event) => {
		event.preventDefault();
		axios
			.post("http://localhost:3001/register", {
				email,
				password
			})
			.then((res) => {
				createAccount(res.data.user.id, res.data.accessToken);
				setEmail("");
				setPassword("");
				setCreateAccountModal(false);
			})
			.catch(() => alert("Something went wrong."));
	};

	const modalStyle = {
		position: "absolute",
		top: "50%",
		left: "50%",
		width: { xs: "60%", sm: "auto" },
		transform: "translate(-50%, -50%)",
		p: 4,
		boxShadow: 5,
		bgcolor: "#fff"
	};

	const closeModal = () => {
		setCreateAccountModal(false);
		setEmail("");
		setPassword("");
	};

	const switchToLogin = () => {
		closeModal();
		setShowLoginModal(true);
	};

	return (
		<Modal
			open={createAccountModal}
			onClose={closeModal}
			disableScrollLock={true}
		>
			<Box sx={modalStyle}>
				<IconButton
					onClick={closeModal}
					tabIndex={-1}
					style={{
						position: "absolute",
						right: 0,
						top: 0
					}}
				>
					<CancelIcon />
				</IconButton>
				<form onSubmit={handleCreateAccount}>
					<Stack spacing={2}>
						<h3>Create account</h3>
						<TextField
							required
							label="Email"
							type="email"
							value={email}
							onChange={({ target }) => setEmail(target.value)}
						/>
						<TextField
							required
							label="Password"
							type="password"
							value={password}
							onChange={({ target }) => setPassword(target.value)}
						/>
						<Button variant="contained" type="submit">
							Create account
						</Button>
					</Stack>
				</form>
				<Stack
					sx={{
						paddingTop: "10px",
						flexDirection: { xs: "column", md: "row" },
						justifyContent: "center",
						alignItems: "center"
					}}
				>
					<h5>Already registered?</h5>
					<Button size="small" onClick={switchToLogin}>
						Login
					</Button>
				</Stack>
			</Box>
		</Modal>
	);
};

export default CreateAccount;
