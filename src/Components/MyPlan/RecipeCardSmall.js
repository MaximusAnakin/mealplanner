import React, { useEffect, useState } from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {
	Card,
	CardMedia,
	CardContent,
	Collapse,
	IconButton,
	Typography,
	Stack,
	styled
} from "@mui/material";
import DeleteButton from "./DeleteButton";
import axios from "axios";

const ExpandMore = styled((props) => {
	const { ...other } = props;
	return <IconButton {...other} />;
})(({ theme, expand }) => ({
	transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
	marginLeft: "auto",
	transition: theme.transitions.create("transform", {
		duration: theme.transitions.duration.shortest
	})
}));

const RecipeCardSmall = ({ mealId, type, date, recipeId, setReload }) => {
	const [expanded, setExpanded] = useState(false);
	const [recipeData, setRecipeData] = useState({});

	useEffect(() => {
		axios.get(`http://localhost:3001/recipes/${recipeId}`).then((res) => {
			setRecipeData(res.data);
		});
	}, []);

	const handleExpandClick = () => {
		setExpanded(!expanded);
	};

	const typeStyle = {
		background: "purple",
		color: "white",
		padding: "5px",
		borderRadius: "10px"
	};

	const dateStyle = {
		background: "teal",
		color: "white",
		padding: "5px",
		borderRadius: "10px"
	};

	return (
		<div>
			<Card sx={{ width: { xs: "350px", sm: "500px" } }}>
				<Stack direction="row">
					<CardMedia
						component="img"
						sx={{ height: "90px", width: "90px" }}
						image={recipeData.image}
						alt={recipeData.title + " recipe"}
					/>
					<Stack
						justifyContent="space-between"
						sx={{ width: "100%", p: "10px" }}
					>
						<div
							style={{
								display: "flex",
								justifyContent: "space-between",
								width: "100%"
							}}
						>
							<div style={{ display: "flex" }}>
								<h5 style={typeStyle}>{type}</h5>
								<h5 style={dateStyle}>{date}</h5>
							</div>
							<DeleteButton
								mealId={mealId}
								setReload={setReload}
							/>
						</div>
						<div
							style={{
								display: "flex",
								justifyContent: "space-between",
								width: "100%"
							}}
						>
							<h4>{recipeData.title}</h4>
							<ExpandMore
								expand={expanded ? 1 : 0}
								onClick={handleExpandClick}
								sx={{ p: 0 }}
							>
								<ExpandMoreIcon />
							</ExpandMore>
						</div>
					</Stack>
				</Stack>

				<Collapse in={expanded} timeout="auto" unmountOnExit>
					<CardContent>
						<Typography>{recipeData.stepList}</Typography>
					</CardContent>
				</Collapse>
			</Card>
		</div>
	);
};

export default RecipeCardSmall;
