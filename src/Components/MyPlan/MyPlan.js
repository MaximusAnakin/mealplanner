import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import RecipeCardSmall from "./RecipeCardSmall";
import { Stack } from "@mui/material";
import { UserContext } from "../UserContext";

const MyPlan = () => {
	const [planData, setPlanData] = useState([]);
	const [reload, setReload] = useState();
	const { userId, accessToken } = useContext(UserContext);

	const emptyPlan = {
		position: "absolute",
		top: "40%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		textAlign: "center"
	};
	useEffect(() => {
		axios({
			method: "get",
			url: "http://localhost:3001/meals",
			params: {
				userId: `${userId}`
			},
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/JSON; charset=utf-8"
			}
		})
			.then((res) => {
				if (res.data !== undefined) {
					const sortedData = res.data.sort((a, b) =>
						a.dateISO.localeCompare(b.dateISO)
					);
					setPlanData(sortedData);
				}
				setReload(false);
			})
			.catch(() => console.log("Error: If list is empty, should be fixed in the backend."));
	}, [reload]);

	return (
		<>
			<Stack alignItems="center" padding="20px" spacing={1}>
				{planData.length === 0 && (
					<div style={emptyPlan}>
						<h1>Your plan is empty.</h1>
						<h3>Add some juicy recipes.</h3>
					</div>
				)}
				{planData.length !== 0 && (
					<h2 style={{ padding: "20px" }}>MY PLAN</h2>
				)}
				{planData.map((meal) => (
					<RecipeCardSmall
						key={meal.id}
						mealId={meal.id}
						stepList={meal.stepList}
						type={meal.type}
						date={meal.date}
						recipeId={meal.recipeId}
						setReload={setReload}
					/>
				))}
			</Stack>
		</>
	);
};

export default MyPlan;
