import React, { useContext } from "react";
import axios from "axios";
import { Button } from "@mui/material";
import { UserContext } from "../UserContext";

const DeleteButton = ({ mealId, setReload }) => {
	const { accessToken } = useContext(UserContext);

	const handleDelete = () => {
		axios({
			method: "delete",
			url: `http://localhost:3001/meals/${mealId}`,
			headers: {
				Authorization: "Bearer " + accessToken,
				"Content-type": "application/JSON; charset=utf-8"
			}
		})
			.then(() => setReload(true))
			.catch(() => alert("Something went wrong"));
	};

	return (
		<div>
			<Button onClick={handleDelete} sx={{ p: 0 }} size="small">
				Remove
			</Button>
		</div>
	);
};

export default DeleteButton;
