import React from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./Home/Home";
import MyPlan from "./MyPlan/MyPlan";
import ProtectedRoute from "./ProtectedRoute";

const reactRouter = () => (
	<Routes>
		<Route path="/" element={<Home />} />
		<Route
			path="/myplan"
			element={
				<ProtectedRoute>
					<MyPlan />
				</ProtectedRoute>
			}
		/>
		<Route path="*" element={<>Page not found</>} />
	</Routes>
);
export default reactRouter;
