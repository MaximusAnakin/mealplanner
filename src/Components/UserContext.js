import React, { useEffect, useState, createContext } from "react";

export const UserContext = createContext();

export const UserProvider = ({ children }) => {
	const [userId, setUserId] = useState(null);
	const [accessToken, setAccessToken] = useState(null);
	const [showLoginModal, setShowLoginModal] = useState(false);
	const [createAccountModal, setCreateAccountModal] = useState(false);

	useEffect(() => {
		const loggedInUser = window.localStorage.getItem("MealPlannerUserId");
		const userAccessToken = window.localStorage.getItem("MealPlannerUserToken");
		if (loggedInUser) {
			setUserId(loggedInUser);
			setAccessToken(userAccessToken);
		}
	}, []);

	const createAccount = (usreId, accessToken) => {
		window.localStorage.setItem("MealPlannerUserToken", accessToken);
		window.localStorage.setItem("MealPlannerUserId", userId);
		setUserId(userId);
		setAccessToken(accessToken);
		setCreateAccountModal(false);
	};

	const login = (userId, accessToken) => {
		window.localStorage.setItem("MealPlannerUserToken", accessToken);
		window.localStorage.setItem("MealPlannerUserId", userId);
		setUserId(userId);
		setAccessToken(accessToken);
		setShowLoginModal(false);
	};

	const logout = () => {
		window.localStorage.removeItem("MealPlannerUserToken");
		window.localStorage.removeItem("MealPlannerUserId");
		setUserId(null);
		setAccessToken(null);
	};

	return (
		<UserContext.Provider
			value={{
				userId,
				accessToken,
				showLoginModal,
				setShowLoginModal,
				createAccountModal,
				setCreateAccountModal,
				createAccount,
				login,
				logout
			}}
		>
			{children}
		</UserContext.Provider>
	);
};
