import React from "react";
import { ThemeProvider } from "@mui/material";
import AppContainer from "./Components/AppContainer";
import theme from "./Theme";
import { UserProvider } from "./Components/UserContext";

const App = () => (
	<ThemeProvider theme={theme}>
		<UserProvider>
			<AppContainer />
		</UserProvider>
	</ThemeProvider>
);

export default App;
